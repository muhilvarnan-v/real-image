# Real Image Challenge 2016

## Web application endpoint

   * https://real-image.herokuapp.com

## Api Documentation

   * https://real-image.herokuapp.com/api-docs

### Pages
    
   * Distributors - List all Distributors
   * Create Distributor - Create a distributor
   * Check Permission - Check region permission of a distributor


### Modules used
   * nodejs
   * node-swagger
   * swagger-ui
   * reactjs
   * redux
   * webpack
