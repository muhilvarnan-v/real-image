    const csv=require('csvtojson')
const jsonfile = require('jsonfile')

const csvFilePath='./cities.csv'

const country = []

const province = {}

const data = {}

csv()
.fromFile(csvFilePath)
.on('json',(jsonObj)=>{
    // combine csv header row and csv line to a json object 
    // jsonObj.a ==> 1 or 4 
    //console.log(jsonObj)
    if (!data.hasOwnProperty(jsonObj['Country Code'])){
    	country.push({
    		"name":jsonObj["Country Name"],
    		"id":jsonObj["Country Code"]
    	})
        province[jsonObj['Country Code']] = []
    	data[jsonObj['Country Code']] = {}	
    }
    if (!data[jsonObj['Country Code']].hasOwnProperty(jsonObj['Province Code'])){
    	province[jsonObj['Country Code']].push({
            "name":jsonObj['Province Name'],
            "id":jsonObj["Province Code"]
        })
    	data[jsonObj['Country Code']][jsonObj["Province Code"]] = []
    	
    }  
    
    data[jsonObj['Country Code']][jsonObj["Province Code"]].push({
    		"name":jsonObj['City Name'],
    		"id":jsonObj['City Code']
    })
})
.on('done',(error)=>{
    console.log(data["IN"])
    jsonfile.writeFileSync("data/country.json", country)
  	jsonfile.writeFileSync("data/province.json", province)
    jsonfile.writeFileSync("data/city.json", data)  
})
