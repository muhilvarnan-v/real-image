'use strict';
var util = require('util');
var JsonDB = require('node-json-db');
var randomstring = require("randomstring");


module.exports = {
  create: create,
  list: list,
  permission:permission
};

function arrayUnique(array) {
    var a = array.concat();
    for(var i=0; i<a.length; ++i) {
        for(var j=i+1; j<a.length; ++j) {
            if(a[i] === a[j])
                a.splice(j--, 1);
        }
    }

    return a;
}
function merge_payload(child_payload, parent_payload){
  child_payload['include']['country'] = arrayUnique(child_payload['include']['country'].concat(parent_payload['include']['country']))
  child_payload['include']['province'] = arrayUnique(child_payload['include']['province'].concat(parent_payload['include']['province']))
  child_payload['include']['city'] = arrayUnique(child_payload['include']['city'].concat(parent_payload['include']['city']))
  child_payload['exclude']['country'] = arrayUnique(child_payload['exclude']['country'].concat(parent_payload['exclude']['country']))
  child_payload['exclude']['province'] = arrayUnique(child_payload['exclude']['province'].concat(parent_payload['exclude']['province']))
  child_payload['exclude']['city'] = arrayUnique(child_payload['exclude']['city'].concat(parent_payload['exclude']['city']))
  return child_payload
}


function set_permissions(parent_distributor, payload) {
  var db = new JsonDB("distributor", true, false);
  var parent_distributor_data = db.getData("/distributor/"+parent_distributor);
  payload = merge_payload(payload, parent_distributor_data)
  if(parent_distributor_data.hasOwnProperty('parent_distributor')) {
    payload = set_permissions(parent_distributor_data['parent_distributor'], payload);
  }
  return payload
}

function frame_empty_set(data) {
  if(typeof data['include'] == "undefined") {
      data['include']=[]
  } 

  if(typeof data['exclude'] == "undefined") {
      data['exclude']=[]
  } 

  if(typeof data['include']['country'] == "undefined") {
    data['include']['country'] = []
  }
  if(typeof data['include']['province'] =="undefined") {
    data['include']['province'] = []
  }
  
  if(typeof data['include']['city'] =="undefined") {
    data['include']['city'] = []
  }
  
  if(typeof data['exclude']['country'] =="undefined") {
    data['exclude']['country'] = []
  }

  if(typeof data['exclude']['province'] =="undefined") {
    data['exclude']['province'] = []
  }

  if(typeof data['exclude']['city'] =="undefined") {
    data['exclude']['city'] = []
  }
  return data
}
function create(req, res) {
  var db = new JsonDB("distributor", true, false);
  var data = req.swagger.params.distributor.value;
  data = frame_empty_set(data);
  if (req.swagger.params.distributor.value.hasOwnProperty('parent_distributor')) {
      data = set_permissions(req.swagger.params.distributor.value['parent_distributor'], data);
  }
  var id = randomstring.generate();
  data['id'] = id;
  
  db.push("/distributors[]", data, true);
  db.push("/distributor/"+id, data);
  res.json({
    "message":"distributor created successfully"
  });
}


function list(req, res) {
  var db = new JsonDB("distributor", true, false);
  res.json(db.getData("/distributors"));
}

function permission(req, res) {
  var db = new JsonDB("distributor", true, false);
  var data = db.getData("/distributor/"+req.swagger.params.permission.value['distributor_id']);
  var status = false
  if(req.swagger.params.permission.value.hasOwnProperty('country')) {
      if(data['include']['country'].indexOf(req.swagger.params.permission.value['country'])>-1) {
        status = true
      }
  } 

  if(req.swagger.params.permission.value.hasOwnProperty('province')) {
      var con_pro = req.swagger.params.permission.value['country']+"-"+req.swagger.params.permission.value['province']
      if(status) {
        if(data['exclude']['province'].indexOf(con_pro)>-1) {
          status = false
        }
      } else {
         if(data['include']['province'].indexOf(con_pro)>-1) {
          status = true
        } 
      }
  }

  if(req.swagger.params.permission.value.hasOwnProperty('city')) {
      var con_pro_city = req.swagger.params.permission.value['country']+"-"+req.swagger.params.permission.value['province']+"-"+req.swagger.params.permission.value['city']
      console.log(status)
      if(status) {
        if(data['exclude']['city'].indexOf(con_pro_city)>-1) {
          status = false
        }
      } else {
         if(data['include']['city'].indexOf(con_pro_city)>-1) {
          status = true
        } 
      }
  }

  res.json({
    "status":status
  });    

}
