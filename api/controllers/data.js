'use strict';
var util = require('util');
var jsonfile = require('jsonfile')

module.exports = {
  country: country,
  province: province,
  city:city
};

function country(req, res) {
  var file = 'data/country.json';
  jsonfile.readFile(file, function(err, obj) {
    res.json(obj);
  });
}

function province(req, res) {
  var file = 'data/province.json';
  jsonfile.readFile(file, function(err, obj) {
    if(obj.hasOwnProperty(req.swagger.params.country_code.value)) {
      res.json(obj[req.swagger.params.country_code.value]);
    } else{
      res.json([]);
    }
    
  });
}

function city(req, res) {
  var file = 'data/city.json';
  jsonfile.readFile(file, function(err, obj) {
    if(obj.hasOwnProperty(req.swagger.params.country_code.value)) {
      obj = obj[req.swagger.params.country_code.value]
      if(obj.hasOwnProperty(req.swagger.params.province_code.value)) {
        res.json(obj[req.swagger.params.province_code.value]);
      } else {
          res.json([]);
      }
      
    } else{
      res.json([]);
    }
    
  });
}