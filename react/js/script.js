import React from "react";
import ReactDOM from "react-dom";
import {Router, Route, IndexRoute, hashHistory } from "react-router";

import Layout from "./pages/Layout";
import Distributors from "./pages/Distributors";
import CreateDistributor from "./pages/CreateDistributor";
import Permission from "./pages/Permission";
import { Provider } from "react-redux";
import store from "./store";
const app = document.getElementById("app");

ReactDOM.render(
	<Provider store={store}>
	<Router history={hashHistory}>
	 <Route path="/" component={Layout}>
	 	<IndexRoute component={Distributors}></IndexRoute>
	 	<Route path="create" component={CreateDistributor}></Route>
	 	<Route path="permission" component={Permission}></Route>
 	 </Route>
	</Router>
	</Provider>, app);	