import React from "react";
import Country from "../components/region/Country";
import Province from "../components/region/Province";
import City from "../components/region/City";
import { fetchCountry, fetchProvince, fetchCity } from "../actions/region"
import { connect } from "react-redux"

@connect((store) => {
	return {
		countries: store.region.countries,
		provinces: store.region.provinces,
		cities: store.region.cities
	}
})
export default class Item extends React.Component {
	
	constructor() {
      super();
      this.state = {
      	show_province:false,
      	show_city:false
      }
	}

	componentWillMount() {
		this.props.dispatch(fetchCountry())
	}

	countryChange(e) {
		this.setState({show_province:false,show_city:false});
		if(e.target.value!="Select") {
			this.props.dispatch(fetchProvince(e.target.value))
			this.setState({show_province:true, country:e.target.value})
			var index = e.nativeEvent.target.selectedIndex;
			this.props.setData({country:{id:e.target.value, name:e.nativeEvent.target[index].text}, province:null, city:null})
		} else {
			this.props.setData({country:null})
		}
	}

	provinceChange(e) {
		this.setState({show_city:false, province:null})
		if(e.target.value!="Select") {
			console.log(this.state)
			this.props.dispatch(fetchCity(this.state.country, e.target.value))
			this.setState({show_city:true})
			var index = e.nativeEvent.target.selectedIndex;
			this.props.setData({province:{id:e.target.value, name:e.nativeEvent.target[index].text}, city:null})
		} else {
			this.props.setData({province:null})
		}
	}
	cityChange(e){
		if(e.target.value!="Select") {
			var index = e.nativeEvent.target.selectedIndex;
			this.props.setData({city:{id:e.target.value, name:e.nativeEvent.target[index].text}})
		} else {
			this.props.setData({city:null})
		}
	}

	render() {
		const {countries, provinces, cities} = this.props
		return (
			<div class="region-holder">
				<Country countries={countries} countryChange={this.countryChange.bind(this)} />
				{this.state.show_province ? <Province provinceChange={this.provinceChange.bind(this)} provinces={provinces} /> : null }
				{this.state.show_city ? <City cities={cities} cityChange={this.cityChange.bind(this)}/> : null }
			</div>
		)
	}
}
