import React from "react";
import { IndexLink, Link } from "react-router";

export default class Nav extends React.Component {
	render() {
		return (
			<ul id="navigation">
				<li><IndexLink to="/">Distributors</IndexLink></li>
				<li><Link to="create">Create Distributor</Link></li>
				<li><Link to="permission">Distributor Permission</Link></li>
			</ul>
		)
	}
}