import React from "react";


export default class Item extends React.Component {
	render() {
		return (
			<tr>
				<td width="10%">{this.props.sno}</td>
				<td width="15%">{this.props.data.name}</td>
				<td width="15%">{this.props.data.parent_name}</td>
				<td width="30%">
				{this.props.data.payload.include.map(function(value, key) {
            				return <p key={key}>{value}</p>;
        		})}	
				</td>
				<td width="30%">
					{this.props.data.payload.exclude.map(function(value, key) {
            				return <p key={key}>{value}</p>;
        			})}	
				</td>
			</tr>
		)
	}
}