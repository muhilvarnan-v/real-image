import React from "react";

import { connect } from "react-redux"

import { fetchDistributor } from "../../actions/distributor"

@connect((store) => {
	return {
		distributors: store.distributor.distributors
	}
})
export default class SelectDistributor extends React.Component {

	componentWillMount() {
		this.props.dispatch(fetchDistributor())
	}

	handleChange(e) {
		this.props.changeDistributor(e)
	}
	render() {
		const {distributors} = this.props
		const mappedDistributor = distributors.map((item, key)=> <option key={key} value={item.id}>{item.name}</option>)
		return (
			<select onChange={this.handleChange.bind(this)}>
				<option>Select</option>
				{mappedDistributor}
			</select>
		)
	}
}
