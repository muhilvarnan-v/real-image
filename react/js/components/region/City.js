import React from "react";

export default class City extends React.Component {
	handleChange(e) {
		this.props.cityChange(e)
	}

	render() {		
		const {cities} = this.props
		const mappedCities = cities.map((item, key)=> <option key={key} value={item.id}>{item.name}</option>)
		return (
      	<div class="region country">
  			<select onChange={this.handleChange.bind(this)}>
  				<option defaultValue="Select">Select City</option>
  				{mappedCities}
  			</select>
      	</div>
		)
	}
}
