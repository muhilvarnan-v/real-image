import React from "react";

export default class Province extends React.Component {

	handleChange(e) {
		this.props.provinceChange(e)
	}

	render() {
		const {provinces} = this.props
		const mappedProvinces = provinces.map((item, key)=> <option key={key} value={item.id}>{item.name}</option>)
		return (
	      <div class="region country">
	  			<select onChange={this.handleChange.bind(this)}>
	  				<option defaultValue="Select">Select Province</option>
	  				{mappedProvinces}
	  			</select>
	      </div>
		)
	}
}
