import React from "react";

export default class Country extends React.Component {


	handleChange(e) {
		this.props.countryChange(e)
	}

	render() {
		const {countries} = this.props
		const mappedCountries = countries.map((item, key)=> <option key={key} value={item.id}>{item.name}</option>)
		return (
      <div class="region country">
  			<select onChange={this.handleChange.bind(this)}>
  				<option defaultValue="Select">Select Country</option>
  				{mappedCountries}
  			</select>
      </div>
		)
	}
}
