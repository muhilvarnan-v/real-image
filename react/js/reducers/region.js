export default function reducer(state={
    countries: [],
    provinces: [],
    cities:[],
    error: null,
    result:null
  }, action) {

    switch (action.type) {
      case "FETCH_COUNTRY": {
        return {...state,}
      }
      case "FETCH_COUNTRY_REJECTED": {
        return {...state, error: action.payload}
      }
      case "FETCH_COUNTRY_FULFILLED": {
        return {
          ...state,
          countries: action.payload,
        }
      }
      case "FETCH_PROVINCE": {
        return {...state,}
      }
      case "FETCH_PROVINCE_REJECTED": {
        return {...state, error: action.payload}
      }
      case "FETCH_PROVINCE_FULFILLED": {
        return {
          ...state,
          provinces: action.payload,
        }
      }
      case "FETCH_CITY": {
        return {...state,}
      }
      case "FETCH_CITY_REJECTED": {
        return {...state, error: action.payload}
      }
      case "FETCH_CITY_FULFILLED": {
        return {
          ...state,
          cities: action.payload,
        }
      }
      case "CHECK_PERMISSION_REJECTED": {
        return {...state, error: action.payload}
      }
      case "CHECK_PERMISSION_FULFILLED": {
        return {
          ...state,
          result: action.payload,
        }
      }
      case "CLEAR_RESULT": {
        return {
          ...state,
          result: null,
        }
      }

    }

    return state
}
