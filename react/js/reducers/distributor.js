export default function reducer(state={
    distributors: [],
    error: null,
    result:null
  }, action) {

    switch (action.type) {
      case "FETCH_DISTRIBUTOR": {
        return {...state,}
      }
      case "FETCH_DISTRIBUTOR_REJECTED": {
        return {...state, error: action.payload}
      }
      case "FETCH_DISTRIBUTOR_FULFILLED": {
        return {
          ...state,
          distributors: action.payload,
        }
      }
      case "CREATE_DISTRIBUTOR_REJECTED": {
        return {...state, error: action.payload}
      }
      case "CREATE_DISTRIBUTOR_FULFILLED": {
        return {
          ...state,
          result: action.payload,
        }
      }
    }

    return state
}