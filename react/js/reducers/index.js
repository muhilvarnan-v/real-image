import { combineReducers } from "redux";

import distributor from "./distributor";
import region from "./region";

export default combineReducers({
	distributor,
  region
});
