import axios from "axios";

export function fetchCountry() {
  return function(dispatch) {
    axios.get("/country")
      .then((response) => {
        dispatch({type: "FETCH_COUNTRY_FULFILLED", payload: response.data})
      })
      .catch((err) => {
        dispatch({type: "FETCH_COUNTRY_REJECTED", payload: err})
      })
  }
}

export function fetchProvince(countryCode) {
  return function(dispatch) {
    axios.get("/province/"+countryCode)
      .then((response) => {
        dispatch({type: "FETCH_PROVINCE_FULFILLED", payload: response.data})
      })
      .catch((err) => {
        dispatch({type: "FETCH_PROVINCE_REJECTED", payload: err})
      })
  }
}

export function fetchCity(countryCode, provinceCode) {
  return function(dispatch) {
    axios.get("/city/"+countryCode+"/"+provinceCode)
      .then((response) => {
        dispatch({type: "FETCH_CITY_FULFILLED", payload: response.data})
      })
      .catch((err) => {
        dispatch({type: "FETCH_CITY_REJECTED", payload: err})
      })
  }
}


export function checkPermission(data) {
  
  return function(dispatch) {
  	const params = {
  		"distributor_id": data.distributor_id,
  		"country": data.country['id']
	}
	if(data.province) {
		params['province'] = data.province['id']
	}
	if(data.city) {
		params['city'] = data.city['id']
	}
	console.log(params);
    axios.post("distributor/permission", params, {headers:{'Content-Type': 'application/json'}})
      .then((response) => {
        dispatch({type: "CHECK_PERMISSION_FULFILLED", payload: response.data})
      })
      .catch((err) => {
        dispatch({type: "CHECK_PERMISSION_REJECTED", payload: err})
      })
  }
}

export function clearResult() { 
   return function(dispatch) {
    dispatch({type: "CLEAR_RESULT"})
  }
}