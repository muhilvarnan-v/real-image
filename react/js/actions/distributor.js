import axios from "axios";

export function fetchDistributor() {
  return function(dispatch) {
    axios.get("/distributor")
      .then((response) => {
        dispatch({type: "FETCH_DISTRIBUTOR_FULFILLED", payload: response.data})
      })
      .catch((err) => {
        dispatch({type: "FETCH_DISTRIBUTOR_REJECTED", payload: err})
      })
  }
}

export function createDistributorAction(data) {
  return function(dispatch) {
  	const params = {
  		'name':data.name
  	}
  	if(data.parent_distributor) {
  		params['parent_distributor'] = data.parent_distributor
  		params['parent_name'] = data.parent_name
  	}
  	params['payload'] = {
  		'include':[],
  		'exclude':[]
  	}
  	if(data.includes.length) {
  		params['include'] = {
  			'country':[],
  			'province':[],
  			'city':[]
  		}
  		var includePayload = []
  		var str = null
  		var str_code = null
  		for(var i=0; i<data.includes.length; i++) {
  			const include = data.includes[i]
  			str = include.country.name
  			str_code = include.country.id
  			if(include.province) {
  				str = str + "-" + include.province.name
  				str_code = str_code +"-"+ include.province.id
  			}
  			if(include.city) {
  				str = str + "-" + include.city.name
  				str_code = str_code+"-"+ include.city.id
  			}
  			const type = (str_code.match(/-/g) || []).length
  			switch(type){
  				case 0: {
  					params['include']['country'].push(str_code)
  					break;
  				} 
  				case 1: {
  					params['include']['province'].push(str_code)
  					break;
  				} 
  				case 2: {
  					params['include']['city'].push(str_code)
  					break;
  				}
  			}
  			includePayload.push(str)
  		}
  		params['payload']['include'] = includePayload
  	}
  	if(data.excludes.length) {
  		params['exclude'] = {
  			'country':[],
  			'province':[],
  			'city':[]
  		}
  		var excludePayload = []
  		var str = null
  		var str_code = null
  		for(var i=0; i<data.excludes.length; i++) {
  			const exclude = data.excludes[i]
  			str = exclude.country.name
  			str_code = exclude.country.id
  			if(exclude.province) {
  				str = str + "-" + exclude.province.name
  				str_code = str_code+"-"+ exclude.province.id
  			}
  			if(exclude.city) {
  				str = str + "-" + exclude.city.name
  				str_code = str_code+"-"+ exclude.city.id
  			}
  			const type = (str_code.match(/-/g) || []).length
  			switch(type){
  				case 0: {
  					params['exclude']['country'].push(str_code)
  					break;
  				} 
  				case 1: {
  					params['exclude']['province'].push(str_code)
  					break;
  				} 
  				case 2: {
  					params['exclude']['city'].push(str_code)
  					break;
  				}
  			}
  			excludePayload.push(str)
  		}
  		params['payload']['exclude'] = excludePayload
  	}
  	console.log(params)
  	axios.post("/distributor", params, {headers:{'Content-Type': 'application/json'}})
      .then((response) => {
        dispatch({type: "CREATE_DISTRIBUTOR_FULFILLED", payload: response.data})
      })
      .catch((err) => {
        dispatch({type: "CREATE_DISTRIBUTOR_REJECTED", payload: err})
      })
  }
}
