import React from "react";
import Nav from "../components/Nav";

export default class Layout extends React.Component {
	
	render() {
		return (
		<div>
			<div id="home" class="content">
				{this.props.children}
			</div>
			<div id="header">
				<h1>Real Image Challenge 2016</h1>
				<Nav />
			</div>

		</div>
		)
	}
}