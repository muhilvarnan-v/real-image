import React from "react";
import SelectDistributor from "../components/distributors/SelectDistributor";
import RegionHolder from "../components/RegionHolder";
import { checkPermission, clearResult } from "../actions/region";
import { connect } from "react-redux"

@connect((store) => {
	return {
		result: store.region.result
	}
})
export default class Permission extends React.Component {
		
	constructor() {
      super();
      this.state = {
      	distributor_id:null,
      	country:null,
      	province:null,
      	city:null,
      	error:null,
      	show_result:null
      }
	}
	
	changeDistributor(e) {
		if(e.target.value!="Select") {
			this.setState({distributor_id:e.target.value,show_result:false})
		} else {
			this.setState({distributor_id:null, show_result:false})
		}
	}

	setData(data) {
		this.setState({show_result:false})
		this.setState(data);
	}

	checkPermission(e) {
		this.props.dispatch(clearResult())
		this.setState({show_result:true})
		if(this.state.distributor_id==null) {
			this.setState({error:"Please select a distributors", show_result:false})
		} else if(this.state.country==null) {
			this.setState({error:"Please select a country", show_result:false})
		} else {
			this.setState({error:null})
			this.props.dispatch(checkPermission(this.state))
		}
	}

	render() {
		const {result} = this.props
		return (
		<div>
			<h2>Distributor Permission</h2>
			<SelectDistributor changeDistributor={this.changeDistributor.bind(this)} />
			<RegionHolder setData={this.setData.bind(this)}/>
			<div class="clear"></div>
			<button type="button" onClick={this.checkPermission.bind(this)}>Check</button>
			{ this.state.error ? <p>{this.state.error}</p> : null }
			{ result && this.state.show_result ? <p class="check_result">Distributor has {!result.status ? <span>no</span>: null } permission for the selected region </p>: null }
		</div>
		)
	}
}