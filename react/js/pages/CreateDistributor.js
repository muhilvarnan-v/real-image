import React from "react";
import SelectDistributor from "../components/distributors/SelectDistributor";
import RegionHolder from "../components/RegionHolder";
import { createDistributorAction } from "../actions/distributor";
import { connect } from "react-redux"

@connect((store) => {
	return {
		result: store.distributor.result
	}
})
export default class CreateDistributor extends React.Component {

	constructor() {
      super();
      this.state = {
      	name:null,
      	parent_distributor:null,
      	parent_name:null,
      	country:null,
      	province:null,
      	city:null,
      	region_type:'include',
      	includes:[],
      	excludes:[],
      	error:null,
      	show_result:false
      }
	}
	changeName(e) {
		this.setState({name:e.target.value})
	}

	changeDistributor(e) {
		if(e.target.value!="Select") {
			var index = e.nativeEvent.target.selectedIndex;
			this.setState({parent_distributor:e.target.value, parent_name:e.nativeEvent.target[index].text })
		} else {
			this.setState({parent_distributor:null, parent_name:null})
		}
	}

	changeRegionType(e) {
		this.setState({region_type:e.target.value})
	}

	setData(data) {
		this.setState(data);
	}

	removeInclude(key) {
		var newArray = this.state.includes.slice();    
		newArray.splice(key, 1);   
		this.setState({includes:newArray})
	}

	removeExclude(key) {
		var newArray = this.state.excludes.slice();    
		newArray.splice(key, 1);   
		this.setState({excludes:newArray})
	}

	addRegion() {
		if(this.state.country) {
			const data = {
			country: this.state.country,
			city: this.state.city,
			province:this.state.province
			}
			if(this.state.region_type=='include') {
				var newArray = this.state.includes.slice();    
			    newArray.push(data);   
			    this.setState({includes:newArray})
			} else {
				var newArray = this.state.excludes.slice();    
			    newArray.push(data);   
			    this.setState({excludes:newArray})
			}
		}
	}

	createDistributor() {
		if(this.state.name==null) {
			this.setState({error:"Please enter distributor name"})
		} else if(this.state.includes.length==0 && this.state.excludes.length==0) {
			this.setState({error:"Please add region in include or exclude"})
		} else {
			this.setState({error:null, show_result:true})
			console.log(this.state);
			this.props.dispatch(createDistributorAction(this.state))
		}

	}

	render() {
		const mappedInclude = this.state.includes.map((item, key)=> <div key={key}><p>{item.country.name} {item.province ? item.province.name : null } {item.city ? item.city.name : null } <span onClick={this.removeInclude.bind(this, key)}>X</span></p></div>)
		const mappedExclude = this.state.excludes.map((item, key)=> <div key={key}><p>{item.country.name} {item.province ? item.province.name : null } {item.city ? item.city.name : null } <span onClick={this.removeExclude.bind(this, key)}>X</span></p></div>)
		const {result} = this.props
		return (
		<div>
			<h2>Create Distributor</h2>
			<form width="100%">
				<table width="100%" cellSpacing="10">
					<tbody>
					<tr>
						<td width="15%">
							Distributor
						</td>
						<td><input type="text" value={this.state.name} onChange={this.changeName.bind(this)} /></td>
					</tr>
					<tr>
						<td>
							Parent Distributor
						</td>
						<td>
							<SelectDistributor changeDistributor={this.changeDistributor.bind(this)} />
						</td>				
					</tr>
					<tr>
						<td>
							Add Region
						</td>
						<td>
							<select onChange={this.changeRegionType.bind(this)}>
								<option defaultValue="include">Include</option>
								<option value="exclude">Exclude</option>
							</select>
							<button type="button" onClick={this.addRegion.bind(this)}>Add</button>
					
							<RegionHolder setData={this.setData.bind(this)} />
							<div class="clear"></div>
						</td>
					</tr>
					<tr>
						<td>
							Included Region
						</td>
						<td>
							{mappedInclude}
						</td>
					</tr>
					<tr>
						<td>
							Excluded Region
						</td>
						<td>
							{mappedExclude}
						</td>
					</tr>
					</tbody>
				</table>
				<button type="button" onClick={this.createDistributor.bind(this)}>Save</button>
				{this.state.error ? <p>{this.state.error}</p> : null}
				{result && this.state.show_result ? <p class="check_result">{result.message}</p>: null }
			</form>
		</div>
		)
	}
}