import React from "react";


import { connect } from "react-redux"

import { fetchDistributor } from "../actions/distributor"

import Item from "../components/distributors/Item"

@connect((store) => {
	return {
		distributors: store.distributor.distributors
	}
})
export default class Disbututor extends React.Component {
	componentWillMount() {
		this.props.dispatch(fetchDistributor())
	}

	render() {
		const {distributors} = this.props
		const mappedDistributor = distributors.map((item, key)=> <Item key={key} data={item} sno={key+1} />)
		return (
		<div>
			<h2>Distribtutors</h2>
			<table width="100%" cellSpacing="10">
					<tbody>
					<tr>
						<td>S.No</td>
						<td>Disbututor</td>
						<td>Parent Disbututor</td>
						<td>Included Region</td>
						<td>Exclude Region</td>
					</tr>
					{mappedDistributor}
					</tbody>
				</table>
			
		</div>
		)
	}
}