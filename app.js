'use strict';

var SwaggerExpress = require('swagger-express-mw');
var express = require('express');
var app = express();
module.exports = app; // for testing

var config = {
  appRoot: __dirname // required config
};

app.use(express.static(__dirname +'/swagger-ui/dist/'))

app.use(express.static(__dirname +'/react/'))

app.get('/',function(req,res){ 
     res.sendFile( __dirname +'/react/index.html');
});

app.get('/api-docs',function(req,res){ 
     res.sendFile( __dirname +'/swagger-ui/dist/api.html');
});

SwaggerExpress.create(config, function(err, swaggerExpress) {
  if (err) { throw err; }

  // install middleware
  swaggerExpress.register(app);

  var port = process.env.PORT || 10010;
  app.listen(port);
});
